const express = require('express');
const session = require('express-session');
const bodyParser = require('body-parser');
const app = express.Router();
const authActions = require('./actions/auth/index');
const recipesActions = require('./actions/recipes/index');
// const mapUrl = require('./utils/url.js');
const multer  = require('multer');
const upload = multer({ dest: 'public/images' });

app.use(session({
  secret: 'ThisIsALongSECRET',
  resave: false,
  saveUninitialized: false,
}));

const routeMap = [
  ['get', '/loadAuth', [], authActions.loadAuth],
  ['post', '/login', [bodyParser.json()], authActions.login],
  ['get', '/logout', [], authActions.logout],

  ['get', '/recipes', [], recipesActions.find],
  ['post', '/recipes', [recipesActions.isLoggedIn, upload.single('picture')], recipesActions.insert],
  ['put', '/recipes/:id', [recipesActions.isLoggedIn, recipesActions.isRecipeOwner, upload.single('picture')], recipesActions.update],
  ['delete', '/recipes/:id', [recipesActions.isLoggedIn, recipesActions.isRecipeOwner], recipesActions.remove]
];

routeMap.forEach(([method, path, middleware, action]) => {
  app[method](path, ...middleware, (req, res) => {
    if (action) {
      action(req)
        .then((result) => {
          if (result instanceof Function) {
            result(res);
          } else {
            res.json(result);
          }
        }, (err) => {
          if (err && err.redirect) {
            res.redirect(err.redirect);
          } else {
            console.error('API ERROR:', pretty.render(err));
            res.status(err.status || 500).json(err);
          }
        });
    } else {
      res.status(404).end('NOT FOUND');
    }
  });
});

module.exports = app;
