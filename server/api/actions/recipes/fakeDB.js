const _ = require('lodash');

function DB() {
  this.idCount = 1;
  this.data = [];
}

DB.prototype.find = function find() {
  return this.data;
}

DB.prototype.findById = function findById(id) {
  return this.data[this.getIndexOfId(id)];
}

DB.prototype.insert = function insert(doc) {
  const newObj = _.assign({}, doc, {_id: (this.idCount++).toString()})
  this.data.push(newObj);
  return newObj;
}

DB.prototype.getIndexOfId = function getIndexOfId(id) {
  const obj = this.data
    .map((val, ind) => ({ind, val}))
    .find(x => x.val._id === id);
  return obj ? obj.ind : -1;
}

DB.prototype.update = function update(id, patch) {
  const ind = this.getIndexOfId(id);
  const rest = _.omit(patch, ['_id']);
  if (ind !== -1) {
    const newObj = _.assign({}, this.data[ind], rest)
    this.data[ind] = newObj;
    return newObj;
  }
}

DB.prototype.remove = function remove(id) {
  const ind = this.getIndexOfId(id);
  if (ind !== -1) {
    const obj = this.data[ind];
    this.data.splice(ind, 1);
    return obj;
  }
}

const db = new DB;

const initRecipes = [
  {
    name: 'MEXICAN BURGERS', owner: 'alice', image: 'mexican-burgers.jpg', calories: '336', prepTime: '5', cookTime: '12',
    description: 'Add some South-of-the-border spice to plain burgers with Mexican Seasoning, a flavorful blend of chili pepper, garlic, onion and cumin. Photo credit: Kristen Doyle from Dine and Dish.',
    ingredients: [{amount: '500', val: 'ground beef'}, {amount: '15', val: 'McCormick® Perfect Pinch® Mexican Seasoning'}, {amount: '250', val: 'hamburger rolls'}],
    steps: [{val: 'Mix ground beef and Seasoning in medium bowl until well blended. Shape into 4 patties.'}, {val: 'Broil or grill over medium heat 4 to 6 minutes per side or until burgers are cooked through (internal temperature of 160°F).'}, {val: 'Serve burgers on rolls. Garnish with desired toppings and condiments.'}]
  },
  {
    name: 'FRENCH TOAST', owner: 'akash', image: 'french-toast.jpg', calories: '99', prepTime: '5', cookTime: '5',
    description: 'Vanilla extract and cinnamon bring a richness of flavor to French toast. Pick your favorite bread - white, Italian, French or whole wheat. Serve with Maple-Flavored Syrup.',
    ingredients: [{amount: '40', val: 'Egg'}, {amount: '6', val: 'McCormick® Pure Vanilla Extract'}, {amount: '3', val: 'McCormick® Cinnamon, Ground'}, {amount: '85', val: 'Milk'}, {amount: '200', val: 'bread'}],
    steps: [{val: 'Beat egg, vanilla and cinnamon in shallow dish. Stir in milk.'}, {val: 'Dip bread in egg mixture, turning to coat both sides evenly.'}, {val: 'Cook bread slices on lightly greased nonstick griddle or skillet on medium heat until browned on both sides.'}]
  },
  {
    name: 'CUPID CUPCAKES', owner: 'anvesh', image: 'cupids-cupcakes.jpg', calories: '200', prepTime: '10', cookTime: '24',
    description: 'Stir the imagination of little cherubs with delightful pink cupcakes, tinted with McCormick® Red Food Color.',
    ingredients: [{amount: '250', val: 'white cake mix'}, {amount: '6', val: 'McCormick® Pure Vanilla Extract'}, {amount: '14', val: 'McCormick® Red Food Color'}],
    steps: [{val: 'Lightly grease or line 24 muffin cups with paper baking cups. Prepare cake mix as directed on package, adding vanilla and red food color to batter.'}, {val: 'Spoon batter into prepared muffin cups, filling each cup 2/3 full.'}, {val: 'Bake as directed on package. Cool completely. Frost with Pink Frost Icing or Very Valentine Glaze.'}]
  },
  {
    name: 'PASTA', owner: 'akash', image: 'sausage-and-peppers-alfredo-pasta.jpg', calories: '131', prepTime: '10', cookTime: '25',
    description: 'This go-to recipe is ready in 30 minutes, so it’s perfect for weeknights when time is short. Combine sausage, red bell pepper, onions and Zatarain’s Alfredo Pasta Dinner Mix, and you’ll have a flavorful family meal in no time.',
    ingredients: [{amount: '15', val: 'olive oil'}, {amount: '30', val: 'smoked sausage'}, {amount: '340', val: 'thinly sliced red bell pepper'}, {amount: '170', val: 'thinly sliced onion'}, {amount: '500', val: 'milk'}, {amount: '340', val: 'water'}],
    steps: [{val: 'Heat oil in large skillet on medium-high heat. Add sausage; cook and stir 5 minutes or until lightly browned. Remove from skillet. Add bell pepper and onion to skillet; cook and stir 2 minutes or until tender-crisp'}, {val: 'Stir in milk, water and Pasta Dinner Mix. Bring to boil, stirring occasionally. Reduce heat to low; cover and simmer 15 minutes or until pasta is tender, stirring occasionally. Stir in sausage'}, {val: 'Remove from heat. Let stand 5 minutes. Stir before serving'}]
  }
];
initRecipes.forEach(r => db.insert(r));

module.exports = db;
