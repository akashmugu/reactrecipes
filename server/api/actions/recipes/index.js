const db = require('./fakeDB');
const _ = require('lodash');

const notAuthorizedErr = {
  status: 403, redirect: '/',
  reason: 'You must be logged in to perform this action'
}

const isLoggedIn = (req, res, next) => {
  require('../auth/loadAuth')(req).then(
    user => {
      if (user) {
        next();
      } else {
        res.status(403).json({
          reason: 'You must be logged in to perform this action'
        });
      }
    }
  );
}
const isRecipeOwner = (req, res, next) => {
  require('../auth/loadAuth')(req).then(
    user => {
      if (db.findById(req.params.id).owner === user.name) {
        next();
      } else {
        res.status(403).json({
          reason: 'Only the user who created the recipe can edit or delete it'
        });
      }
    }
  );
}

function find(req) {
  return Promise.resolve(db.find());
}

function insert(req) {
  const fileObj = {};
  if (req.file && req.file.filename) {
    fileObj.image = req.file.filename;
  }
  return Promise.resolve(db.insert(_.assign({}, JSON.parse(req.body.rest), {owner: req.session.user.name}, fileObj)));
}

function update(req) {
  const fileObj = {};
  if (req.file && req.file.filename) {
    fileObj.image = req.file.filename;
  }
  return Promise.resolve(db.update(req.params.id, _.assign({}, JSON.parse(req.body.rest), fileObj)));
}

function remove(req) {
  return Promise.resolve(db.remove(req.params.id));
}

module.exports = {
  isLoggedIn, isRecipeOwner,
  find, update, remove, insert
};
