const loadAuth = require('./loadAuth');
const login = require('./login');
const logout = require('./logout');

module.exports = { loadAuth, login, logout };
