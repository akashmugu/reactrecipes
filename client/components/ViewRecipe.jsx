import React from 'react';

class ViewRecipe extends React.Component {
  render() {
    const {
      recipe: {
        name, description, owner, image,
        ingredients, steps,
        calories, prepTime, cookTime,
      },
      user, onEdit, onDelete
    } = this.props;
    return (
      <div className="viewPage">

        <div className="row shade panel panel-default">
          <div className="col-md-6 panel-body">
            <img className="pic" src={`/images/${image}`}/>
          </div>
          <div className="col-md-6 panel-body">
            <div className="panel-body">
              <div className="row title">
                <b>{name}</b>
                {user && (user.name === owner) && (
                  <div>
                    <button onClick={onEdit} type='button' className='btn btn-info'>EDIT</button>
                    <button onClick={onDelete} type='button' className='btn btn-danger'>DELETE</button>
                  </div>
                )}
              </div>
              <div className="row author">
                -by <b>{owner}</b>
              </div>
            </div>
            <div className="row">
              <div className="col-md-4 col-sm-4 col-xs-4">
                <div className="row text-center">COOK TIME</div>
                <div className="row text-center"><b>{cookTime} min</b></div>
              </div>
              <div className="col-md-4 col-sm-4 col-xs-4">
                <div className="row text-center">PREP TIME</div>
                <div className="row text-center"><b>{prepTime} min</b></div>
              </div>
              <div className="col-md-4 col-sm-4 col-xs-4">
                <div className="row text-center">CALORIES</div>
                <div className="row text-center"><b>{calories}</b></div>
              </div>
            </div>
            <hr />
            <div className="row panel-body">
              {description}
            </div>
          </div>
        </div>


        <div className="row panel panel-default">
          <div className="panel-heading ">
            <div className="row">
              <div className="col-md-5 col-sm-4"></div>
              <div className="col-md-2 col-sm-4 text-center title">Ingredients</div>
              <div className="col-md-5 col-sm-4"></div>
            </div>
          </div>
          <div className="panel-body">
            <div className="row panel-body">
              {ingredients.map(({amount, val}, ind) => (
                <div key={ind.toString()} className="col-md-4 col-sm-4 col-xs-6 ingredients"><span className="">♨ </span>{amount} gram {val}</div>
              ))}
            </div>
          </div>
        </div>

        <div className="row panel panel-default">
          <div className="panel-heading title text-center">
            Procedure
          </div>
          <div className="panel-body procedure">
            {steps.map(({val}, ind) => (
              <div key={ind.toString()} className="panel-body">
                <b>Step {ind}:</b> {val}
                <hr />
              </div>
            ))}
          </div>
        </div>

      </div>
    );
  }
}

export default ViewRecipe;
