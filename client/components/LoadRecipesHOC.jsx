import React, { Component } from 'react';

import Loader from './LoaderSpinner.jsx';

const loadRecipesHOC = (InnerComp) => {
  return class extends Component {
    componentDidMount() {
      if (!this.props.isRecipesLoaded) {
        this.props.load();
      }
    }

    render() {
      const { isRecipesLoaded, ...rest } = this.props;

      return !isRecipesLoaded ? (
        <Loader />
      ) : (
        <InnerComp {...rest} />
      );
    }
  }
}

export default loadRecipesHOC;
