import React from 'react';
import { withRouter, Route, Link } from 'react-router-dom'

const MenuLink = ({ children, to, activeOnlyWhenExact }) => (
  <Route path={to} exact={activeOnlyWhenExact} children={({ match }) => (
    <li className={match ? 'active' : ''}>
      <Link to={to} className={match ? '' : ''}>{children}</Link>
    </li>
  )}/>
)

class Navbar extends React.Component {
  render() {
    const { user, logout } = this.props;
    return (
      <nav className="navbar navbar-default navbar-fixed-top">
        <div className="container">
          <div className="navbar-header">
            <button type="button" className="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
              <span className="sr-only">Toggle navigation</span>
              <span className="icon-bar"></span>
              <span className="icon-bar"></span>
              <span className="icon-bar"></span>
            </button>
            <a className="navbar-brand" href="/">Recipes</a>
          </div>
          <div id="navbar" className="navbar-collapse collapse">
            <ul className="nav navbar-nav">
              <MenuLink activeOnlyWhenExact={true} to="/">All Recipes</MenuLink>
              <MenuLink activeOnlyWhenExact={true} to="/recipes/create">Create a Recipe</MenuLink>
            </ul>
            <ul className="nav navbar-nav navbar-right">
              {user ? (
                <li className="dropdown">
                  <a href="#" className="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">{user.name} <span className="caret"></span></a>
                  <ul className="dropdown-menu">
                    <li><a href="#" onClick={logout}>Logout</a></li>
                  </ul>
                </li>
              ) : (
                <MenuLink activeOnlyWhenExact={true} to="/login">Login</MenuLink>
              )}
            </ul>
          </div>
        </div>
      </nav>
    );
  }
}

export default Navbar;
