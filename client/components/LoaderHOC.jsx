import React, { Component } from 'react';

import Loader from './LoaderSpinner.jsx';

const loaderHOC = (message) => (InnerComp) => {
  return class extends Component {
    render() {
      const { isLoading, ...rest } = this.props;

      return isLoading ? (
        <Loader
          message={message}
        />
      ) : (
        <InnerComp
          {...rest}
        />
      );
    }
  }
}

export default loaderHOC;
