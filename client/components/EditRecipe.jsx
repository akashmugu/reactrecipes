import React from 'react'
import {Field, FieldArray, reduxForm} from 'redux-form';
import {
  createValidator, required, minLength, maxLength, email, nonEmptyArray,
  number, nonNegative,
  requiredNonZeroLenArray, requiredStepsChildrenNonEmpty, requiredIngredientsChildrenNonEmpty
} from '../services/validation';


const validate = createValidator({
  name: [required],
  cookTime: [required, number, nonNegative],
  prepTime: [required, number, nonNegative],
  calories: [required, number, nonNegative],
  ingredients: [requiredNonZeroLenArray('ingredient'), requiredIngredientsChildrenNonEmpty],
  steps: [requiredNonZeroLenArray('step'), requiredStepsChildrenNonEmpty]
});

const renderField = ({input, label, meta: {touched, error, warning}}) => (
  <div className={"form-group " + (touched ? (error ? "has-error has-feedback" : "has-success has-feedback") : "")}>
    <input {...input} className={"form-control "} placeholder={label} data-toggle="tooltip" title={((touched && error) ? error : "")}/>
    {(touched ? (error ? (<span className="form-control-feedback" aria-hidden="true">✗</span>) : (<span className="form-control-feedback" aria-hidden="true">✓</span>)) : "")}
  </div>
)

class renderImgField extends React.Component {
  handleFileChange(e) {
    e.preventDefault();
    const self = this;
    const {props} = this;
    const files = e.target.files;
    const file = files[0];
    if (!file) {
      alert('File selection error. Try again');
      return;
    }
    props.input.onChange(file);

    var reader = new FileReader();
    reader.onload = function (e) {
      self.previewImage.src = e.target.result;
    }
    reader.readAsDataURL(file);
  }
  render() {
    const {props} = this;
    const {
      input, label, initImage,
      meta: {touched, error, warning}
    } = props;
    return (
      <div>
        <img
          ref={(input) => { this.previewImage = input; }}
          className='pic'
          src={initImage ? `/images/${initImage}` : '#'} alt="preview image"
        />
        <input
          type='file' onChange={this.handleFileChange.bind(this)}
          className="form-control" placeholder={label} onBlur={() => {}}
        />
        {touched && ((error && <span>{error}</span>) || (warning && <span>{warning}</span>))}
      </div>
    );
  }
}

const renderTextArea = ({input, label, meta: {touched, error, warning}}) => (
  <div>
    <textarea {...input} className="form-control" placeholder={label}></textarea>
  </div>
)

const renderStep = (step, ind, fields) => {
  return (
    <div key={ind.toString()} className="row">
      <div className="col-md-1 col-sm-2 col-xs-6 cellSize m">Step {ind+1}:</div>
      <div className="col-md-10 col-sm-9 col-xs-10 m">
        <Field name={`${step}.val`} type="text" component={renderField} label="Describe the step" />
      </div>
      <div className="col-md-1 col-sm-1 col-xs-2 m remove">
        <a className='cursor-pointer' onClick={() => fields.remove(ind)}><span>⊗</span></a>
      </div>
    </div>
  );
}
const renderSteps = ({ fields, meta: {touched, error, warning} }) => {
  return (
    <div className="panel-body">
      <div className="panel-body">
        {fields.map(renderStep)}

        <div className="row">
          <div className="col-md-10 col-sm-9 col-xs-12 err"><b>{error}</b></div>
          <div className="col-md-2 col-sm-3 col-xs-12">
            <button onClick={() => fields.push({})} type="button" className="btn btn-success addbutton">
              <span className="glyphicon glyphicon-plus"></span> Add Step
            </button>
          </div>
        </div>
      </div>
    </div>
  );
}

const renderIngredient = (ingredient, ind, fields) => {
  const [amount, val] = ingredient;
  return (
    <div key={ind.toString()} className="row">
      <div className="col-md-2 col-sm-2 col-xs-6 m">
        <div className="text-center">
          <div className="input-group cell">
            <Field name={`${ingredient}.amount`} type="text" component={renderField} label="Quantity" />
            <div className="input-group-addon">gm</div>
          </div>
        </div>
      </div>
      <div className="col-md-9 col-sm-9 col-xs-10 m">
        <Field name={`${ingredient}.val`} type="text" component={renderField} label="Ingredient name" />
      </div>
      <div className="col-md-1 col-sm-1 col-xs-2 m remove">
        <a className='cursor-pointer' onClick={() => fields.remove(ind)}><span>⊗</span></a>
      </div>
    </div>
  );
}
const renderIngredients = ({ fields, meta: {touched, error, warning} }) => {
  return (
    <div className="panel-body">
      {fields.map(renderIngredient)}

      <div className="row">
        <div className="col-md-10 col-sm-9 col-xs-12 err"><b>{error}</b></div>
        <div className="col-md-2 col-sm-3 col-xs-12">
          <button onClick={() => fields.push({})} type="button" className="btn btn-success addbutton">
            <span className="glyphicon glyphicon-plus"></span> Add Ingredient
          </button>
        </div>
      </div>
    </div>
  );
}

const EditRecipe = props => {
  const {handleSubmit, pristine, reset, submitting} = props
  return (
    <form onSubmit={handleSubmit}>
      <div className="editPage">

        <div className="row shade panel panel-default">
          <div className="col-md-6 col-sm-12 col-xs-12 panel-body">
            {/* <img className="pic" src={`/images/${image}`}/> */}
            <Field
              name="picture" component={renderImgField}
              initImage={props.initialValues && props.initialValues.image} label="Recipe Image"
            />
          </div>
          <div className="col-md-6 col-sm-12 col-xs-12 panel-body">
            <div className="panel-body">
              <div className="row title">
                {/* <b><input value={name} onChange={this.handleNameChange.bind(this)} type="text" className="form-control" placeholder="Recipe Name" /></b> */}
                <Field name="name" type="text" component={renderField} label="Recipe Name" />
              </div>
              <div className="row author">
                {/* -by <a href="#">{owner}</a> */}
              </div>
            </div>
            <div className="row allCells">
              <div className="col-md-4 col-sm-4 col-xs-12 m">
                <div className="row text-center">
                  <div className="input-group cell">
                    {/* <input value={cookTime} onChange={this.handleCooktimeChange.bind(this)} type="text" className="form-control" placeholder="Cook Time" /> */}
                    <Field name="cookTime" type="text" component={renderField} label="Cook Time" />
                    <div className="input-group-addon">min</div>
                  </div>
                </div>
              </div>
              <div className="col-md-4 col-sm-4 col-xs-12 m">
                <div className="row text-center">
                  <div className="input-group cell">
                    {/* <input value={prepTime} onChange={this.handlePreptimeChange.bind(this)} type="text" className="form-control" placeholder="Prep Time" /> */}
                    <Field name="prepTime" type="text" component={renderField} label="Prep Time" />
                    <div className="input-group-addon">min</div>
                  </div>
                </div>
              </div>
              <div className="col-md-4 col-sm-4 col-xs-12 m">
                <div className="row text-center">
                  <div className="input-group cell">
                    {/* <input value={calories} onChange={this.handleCaloriesChange.bind(this)} type="text" className="form-control" placeholder="Calorie" /> */}
                    <Field name="calories" type="text" component={renderField} label="Calorie" />
                    <div className="input-group-addon">cal</div>
                  </div>
                </div>
              </div>
            </div>
            <hr />
            <div className="row panel-body">
              {/* <textarea value={description} onChange={this.handleDescriptionChange.bind(this)} className="form-control" rows="5" placeholder="Description"></textarea> */}
              <Field name="description" rows="5" type="text" component={renderTextArea} label="Description" />
            </div>
          </div>
        </div>


        <div className="row panel panel-default">
          <div className="panel-heading ">
            <div className="row">
              <div className="col-md-5 col-sm-4"></div>
              <div className="col-md-2 col-sm-4 text-center title">Ingredients</div>
              <div className="col-md-5 col-sm-4"></div>
            </div>
          </div>
          <div className="panel-body">
            <FieldArray name="ingredients" component={renderIngredients} />
          </div>
        </div>



        <div className="row panel panel-default">
          <div className="panel-heading ">
            <div className="row text-center title">How to make pizza</div>
          </div>

          <FieldArray name="steps" component={renderSteps} />
        </div>
        {/* <button onClick={() => ((this.props.mode === 'create') ? onCreate : onSave)(this.state)} type='button' className='btn btn-default'>Save</button> */}
      </div>
      <div>
        <button className="btn btn-primary" type="submit" style={{margin: 5 + 'px'}} disabled={submitting}>Submit</button>
        <button className="btn btn-primary" type="button" style={{margin: 5 + 'px'}} disabled={pristine || submitting} onClick={reset}>
          Clear Values
        </button>
      </div>
    </form>
  )
}

export default reduxForm({
  form: 'syncValidation', validate
})(EditRecipe)
