import { createStore, compose, applyMiddleware } from 'redux';
import clientMiddleware from './middleware/clientMiddleware';
import createReducer from './createReducer';

export default function (client) {
  const middleware = [clientMiddleware(client)];
  const composed = [];

  if (process.env.NODE_ENV !== 'production') {
    if (window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ !== undefined) {
      // https://github.com/zalmoxisus/redux-devtools-extension
      composed.push(window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__());
    }
  }

  const store = createStore(createReducer(), compose(applyMiddleware(...middleware), ...composed));

  if (process.env.NODE_ENV !== 'production') {
    // Enable Webpack hot module replacement for reducers
    if (module.hot) {
      module.hot.accept('./createReducer', () => {
        store.replaceReducer(createReducer());
      });
    }
  }

  return store;
}
