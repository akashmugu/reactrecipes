import { combineReducers } from 'redux';
import {
  auth,
  recipes
} from './modules/reducer';

import { reducer as form } from 'redux-form'

export default function createReducer(reducers) {
  return combineReducers({
    auth, recipes,
    form,
    ...reducers
  });
}
