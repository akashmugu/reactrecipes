import _ from 'lodash';

const LOAD = 'react-recipes/recipes/LOAD';
const LOAD_SUCCESS = 'react-recipes/recipes/LOAD_SUCCESS';
const LOAD_FAIL = 'react-recipes/recipes/LOAD_FAIL';
const CREATE = 'react-recipes/recipes/CREATE';
const CREATE_SUCCESS = 'react-recipes/recipes/CREATE_SUCCESS';
const CREATE_FAIL = 'react-recipes/recipes/CREATE_FAIL';
const UPDATE = 'react-recipes/recipes/UPDATE';
const UPDATE_SUCCESS = 'react-recipes/recipes/UPDATE_SUCCESS';
const UPDATE_FAIL = 'react-recipes/recipes/UPDATE_FAIL';
const DELETE = 'react-recipes/recipes/DELETE';
const DELETE_SUCCESS = 'react-recipes/recipes/DELETE_SUCCESS';
const DELETE_FAIL = 'react-recipes/recipes/DELETE_FAIL';

const initialState = {
  loaded: false,
  error: {}
};

export default function reducer(state = initialState, action = {}) {
  switch (action.type) {
    case LOAD:
      return {
        ...state,
        loading: true
      };
    case LOAD_SUCCESS:
      return {
        ...state,
        loading: false,
        loaded: true,
        data: action.result,
        error: null
      };
    case LOAD_FAIL:
      return {
        ...state,
        loading: false,
        loaded: false,
        data: null,
        error: action.error
      };
    case UPDATE:
      return {
        ...state,
        updating: true
      };
    case UPDATE_SUCCESS:
      return {
        ...state,
        updating: false,
        updated: true,
        data: state.data.map(r => ((r._id === action.result._id) ? action.result : r)),
        error: null
      };
    case UPDATE_FAIL:
      return {
        ...state,
        updating: false,
        updated: false,
        error: action.error
      };
    case CREATE:
      return {
        ...state,
        creating: true
      };
    case CREATE_SUCCESS:
      return {
        ...state,
        creating: false,
        created: true,
        data: [...state.data, action.result],
        error: null
      };
    case CREATE_FAIL:
      return {
        ...state,
        creating: false,
        created: false,
        error: action.error
      };
    case DELETE:
      return {
        ...state,
        deleting: true
      };
    case DELETE_SUCCESS:
      return {
        ...state,
        deleting: false,
        deleted: true,
        data: state.data.filter(r => r._id !== action.result._id),
        error: null
      };
    case DELETE_FAIL:
      return {
        ...state,
        deleting: false,
        deleted: false,
        error: action.error
      };
    default:
      return state;
  }
}

export function isLoaded(globalState) {
  return globalState.recipes && globalState.recipes.loaded;
}

export function load() {
  return {
    types: [LOAD, LOAD_SUCCESS, LOAD_FAIL],
    promise: (client) => client.get('/recipes/')
  };
}

export function update(recipe) {
  const { picture, ...rest } = recipe;
  return {
    types: [UPDATE, UPDATE_SUCCESS, UPDATE_FAIL],
    promise: (client) => {
      return client.put(`/recipes/${recipe._id}`, {
        multipart: {
          attach: { picture },
          data: { rest }
        }
      });
    }
  };
}

export function _delete(id) {
  return {
    types: [DELETE, DELETE_SUCCESS, DELETE_FAIL],
    promise: (client) => client.del(`/recipes/${id}`)
  };
}

export function create(recipe) {
  const { picture, ...rest } = recipe;
  return {
    types: [CREATE, CREATE_SUCCESS, CREATE_FAIL],
    promise: (client) => {
      return client.post('/recipes/', {
        multipart: {
          attach: { picture },
          data: { rest }
        }
      });
    }
  };
}
