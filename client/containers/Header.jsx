import React from 'react';
import { withRouter, Route, Link } from 'react-router-dom'
import { connect } from 'react-redux';

import Navbar from '../components/Navbar.jsx';
import { isLoaded as isAuthLoaded, load as loadAuth, logout } from '../store/modules/auth';

@connect(state => ({
  isAuthLoaded: isAuthLoaded(state),
  user: state.auth.user
}), { logout, loadAuth })
class Header extends React.Component {
  componentDidMount() {
    if (!this.props.isAuthLoaded) {
      this.props.loadAuth();
    }
  }

  render() {
    const { user, logout } = this.props;
    return <Navbar user={user} logout={logout} />;
  }
}

export default withRouter(Header);
