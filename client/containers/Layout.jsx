import React from 'react';
import { Switch, Route } from 'react-router-dom';
import Helmet from 'react-helmet';
import Header from './Header';
import Footer from './Footer';
import PrivateRoute from './PrivateRoute';

// Routes
import { Home, Recipe, CreateRecipe, Login } from 'routes';
import Miss404 from 'routes/miss404';

const Layout = () => [
  <Header key="header" />,

  <div key="router" className="container">
    <Switch>
      <Route path="/" component={Home} exact={true} />

      {/* <Route path="/recipes/create" component={CreateRecipe} exact={true} /> */}
      <PrivateRoute path="/recipes/create" component={CreateRecipe} exact={true} />

      <Route path="/recipes/:id" component={Recipe} />
      <Route path="/login" component={Login} exact={true} />
      <Route component={Miss404} />
    </Switch>
  </div>,

  <Footer key="footer" />,
];

export default Layout;
