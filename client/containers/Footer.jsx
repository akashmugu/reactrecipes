import React from 'react';
import { Link } from 'react-router-dom';

const Footer = () => (
  <footer className="footer">
    <div className="container">
      <p className="text-muted text-center">Copyright details go here</p>
    </div>
  </footer>
);

export default Footer;
