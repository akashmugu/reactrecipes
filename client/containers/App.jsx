import React from 'react';
import { Provider } from 'react-redux';
import { BrowserRouter } from 'react-router-dom';
import Layout from './Layout';

import { bgLoadModules } from 'routes';

const supportsHistory = 'pushState' in window.history;

class App extends React.Component {
  componentDidMount() {
    const waitTime = 5000;
    console.log(`Loading other async routes in ${parseInt(waitTime / 1000)} sec`);
    setTimeout(bgLoadModules, waitTime);
  }
  render() {
    return (
      <Provider store={this.props.store}>
        <BrowserRouter forceRefresh={!supportsHistory} keyLength={12}>
          <Layout />
        </BrowserRouter>
      </Provider>
    );
  }
}

export default App;
