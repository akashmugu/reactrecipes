import superagent from 'superagent';

const methods = ['get', 'post', 'put', 'patch', 'del'];

function formatUrl(path) {
  const adjustedPath = path[0] !== '/' ? '/' + path : path;

  // Prepend `/api` to relative URL, to proxy to API server.
  return '/api' + adjustedPath;
}

export default class ApiClient {
  constructor(req) {
    methods.forEach((method) =>
      this[method] = (path, { params, data, multipart } = {}) => new Promise((resolve, reject) => {
        const request = superagent[method](formatUrl(path));

        if (multipart) {
          _.keys(multipart.attach).forEach(key => {
            request.attach(key, multipart.attach[key]);
          });

          _.keys(multipart.data).forEach(key => {
            request.field(key, JSON.stringify(multipart.data[key]));
          });
        } else {
          if (params) {
            request.query(params);
          }

          if (data) {
            request.send(data);
          }
        }

        request.end((err, { body } = {}) => err ? reject(body || err) : resolve(body));
      }));
  }

  empty() {}
}
