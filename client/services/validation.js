const isEmpty = value => value === undefined || value === null || value === '';
const join = (rules) => (value, data) => {
  // return rules.map(rule => rule(value, data)).filter(error => !!error)[0]
  for (var i = 0; i < rules.length; i++) {
    const rule = rules[i];
    const error = rule(value, data);
    if (error) {
      return error;
    }
  }
};

export function email(value) {
  // Let's not start a debate on email regex. This is just for an example app!
  if (!isEmpty(value) && !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(value)) {
    return 'Invalid email address';
  }
}


export function required(value) {
  if (isEmpty(value)) {
    return 'Required';
  }
}

export function requiredNonZeroLenArray(objName) {
  return (value) => {
    if (isEmpty(value) || (Array.isArray(value) && (value.length === 0))) {
      return { _error: `Atleast one ${objName} must be entered` };
    }
  }
}

export function requiredStepsChildrenNonEmpty(value) {
  return value.map((x, x_i) => {
    const memberErrors = {};
    if (isEmpty(x.val)) {
      memberErrors.val = 'Required';
    }
    return memberErrors;
  });
}

export function requiredIngredientsChildrenNonEmpty(value) {
  return value.map((x, x_i) => {
    const memberErrors = {};
    if (isEmpty(x.amount)) {
      memberErrors.amount = 'Required';
    } else if (isNaN(Number(x.amount))) {
      memberErrors.amount = 'Must be a number';
    }
    if (isEmpty(x.val)) {
      memberErrors.val = 'Required';
    }

    return memberErrors;
  });
}

export function minLength(min) {
  return value => {
    if (!isEmpty(value) && value.length < min) {
      return `Must be at least ${min} characters`;
    }
  };
}

export function maxLength(max) {
  return value => {
    if (!isEmpty(value) && value.length > max) {
      return `Must be no more than ${max} characters`;
    }
  };
}

export function number(value) {
  if (isNaN(Number(value))) {
    return 'Must be a number';
  }
}

export function nonNegative(value) {
  if (value < 0) {
    return 'Cannot be a negative number';
  }
}

export function integer(value) {
  if (!Number.isInteger(Number(value))) {
    return 'Must be an integer';
  }
}

export function oneOf(enumeration) {
  return value => {
    if (!~enumeration.indexOf(value)) {
      return `Must be one of: ${enumeration.join(', ')}`;
    }
  };
}

export function match(field) {
  return (value, data) => {
    if (data) {
      if (value !== data[field]) {
        return 'Do not match';
      }
    }
  };
}

export function createValidator(rules) {
  return (data = {}) => {
    const errors = {};
    Object.keys(rules).forEach((key) => {
      const rule = join([].concat(rules[key])); // concat enables both functions and arrays of functions
      const error = rule(data[key], data);
      if (error) {
        errors[key] = error;
      }
    });
    return errors;
  };
}
