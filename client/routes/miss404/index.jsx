import React from 'react';
import Helmet from 'react-helmet';
import PageView from 'containers/PageView';

const Miss404 = (props) => (
  <PageView {...props}>
  <main>
    <Helmet>
      <title>Page not Found</title>
    </Helmet>
    <section className="container text-center py-5">
      <h1>404</h1>
      <h3>Page not found</h3>
    </section>
  </main>
  </PageView>
);

export default Miss404;
