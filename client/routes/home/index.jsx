import React from 'react';
import PageView from 'containers/PageView';
import type { ContextRouter } from 'react-router-dom';
import Helmet from 'react-helmet';
import Style from 'components/Style';

import { Route, Link, withRouter } from 'react-router-dom'
import { connect } from 'react-redux';
import Slider from 'react-slick';

import * as recipeActions from '../../store/modules/recipes';
import loadRecipesHOC from '../../components/LoadRecipesHOC';

function SampleNextArrow(props) {
  const {className, style, onClick} = props
  return (
    <div
      className={className + ' rightArrow'}
      style={{...style}}
      onClick={onClick}
    ></div>
  );
}
function SamplePrevArrow(props) {
  const {className, style, onClick} = props
  return (
    <div
      className={className + ' leftArrow'}
      style={{...style}}
      onClick={onClick}
    ></div>
  );
}

@connect(state => {
  return {
    isRecipesLoaded: recipeActions.isLoaded(state),
    recipes: state.recipes,
    user: state.auth.user
  };
}, recipeActions)
@loadRecipesHOC

class Home extends React.Component {
  constructor(props) {
    super(props);
  }

  handleClick(id) {
    this.props.history.push(`/recipes/${id}`);
  }

  render() {
    const { recipes: { data } } = this.props;

    var settings = {
      customPaging: function(i) {
        return <a><img src={`images/${data[i].image}`} width="25" height="25"/></a>
      },
      dots: true,
      dotsClass: 'slick-dots slick-thumb',
      infinite: true,
      speed: 500,
      slidesToShow: 1,
      slidesToScroll: 1,
      autoplay: false,
      autoplaySpeed: 3000,
      pauseOnHover: true,
      nextArrow: <SampleNextArrow />,
      prevArrow: <SamplePrevArrow />
    };
    if (data.length > 0) {
      return (
        <PageView {...this.props}>
          <Style loader={require('./styles.scss')} />
          <Helmet>
            <meta
              name="description"
              content="ReactRecipes"
            />
          </Helmet>
          <Slider className="homePage" {...settings}>
            {data.map(r => (
              <div key={r._id} onClick={() => this.handleClick(r._id)} className='pic' style={{backgroundImage: `url(images/${r.image})`}}>
                <div className='layer'>
                  <div className="row details">
                    <div className="col-md-2 col-sm-3 col-xs-4">Total Time: <b>{parseInt(Number(r.prepTime) + Number(r.cookTime))} min</b></div>
                    <div className="col-md-8 col-sm-6 col-xs-4"></div>
                    <div className="col-md-2 col-sm-3 col-xs-4">Calories: <b>{r.calories} cal</b></div>
                  </div>
                  <div className="row title"><h2><b>{r.name}</b></h2>
                  </div>
                </div>
              </div>
            ))}
          </Slider>
        </PageView>
      );
    } else {
      return <h1>There are no recipes currently. Try creating one</h1>;
    }

  }
}

export default Home;
