import AR from 'services/asyncRoute';

export const bgLoadModules = () => {
  Promise.all([
    import(/* webpackChunkName: "route-home" */ './home'),
    import(/* webpackChunkName: "route-recipe" */ './recipe'),
    import(/* webpackChunkName: "route-create-recipe" */ './recipe/CreateRecipe'),
    import(/* webpackChunkName: "route-login" */ './login')
  ]).then();
}

// Import causes routes to be code-split
// We have to specify each route name/path in order to be statically analyzed by webpack
export const Home = AR(() => import(/* webpackChunkName: "route-home" */ './home'));
export const Recipe = AR(() => import(/* webpackChunkName: "route-recipe" */ './recipe'));
export const CreateRecipe = AR(() => import(/* webpackChunkName: "route-create-recipe" */ './recipe/CreateRecipe'));
export const Login = AR(() => import(/* webpackChunkName: "route-login" */ './login'));

// Force import during development to enable Hot-Module Replacement
if (process.env.NODE_ENV === 'development' && !ASYNC_ROUTES) {
  require('./home');
  require('./recipe');
  require('./recipe/CreateRecipe');
  require('./login');
}
