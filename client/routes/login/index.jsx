import React from 'react';
import { Route, Link, Redirect } from 'react-router-dom'
import { connect } from 'react-redux';
import PageView from 'containers/PageView';

import * as authActions from '../../store/modules/auth';


@connect(state => ({
  user: state.auth.user
}), authActions)
class Login extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      name: '', redirectToReferrer: false
    };
  }

  componentWillReceiveProps(nextProps) {
    if (!this.props.user && nextProps.user) {
      // just logged in
      this.setState({ redirectToReferrer: true });
    }
  }

  handleNameChange(event) {
    this.setState({ name: event.target.value });
  }

  render() {
    const { from } = this.props.location.state || { from: { pathname: '/' } };
    const { redirectToReferrer } = this.state;

    if (redirectToReferrer) {
      return (
        <Redirect to={from}/>
      );
    }

    return (
      <PageView {...this.props}>
      <div>
        {(from.pathname === '/recipes/create') && (
          <div className="alert alert-danger">
            Please <strong>login</strong> to create a recipe
          </div>
        )}
        <div className="alert alert-info">
          <strong>Note:</strong> This is not a real login. It is created to demonstrate features of this site which need a user account. Choose any username to login
        </div>

        <div className="form-inline">
          <div className="form-group">
            <label htmlFor="exampleInputName2">Username</label>
            <input value={this.state.name} onChange={this.handleNameChange.bind(this)} type="text" className="form-control" placeholder="akash" />
          </div>
          <button onClick={() => this.props.login(this.state.name)} className="btn btn-default">Login</button>
        </div>
      </div>
      </PageView>
    );
  }
}

export default Login;
