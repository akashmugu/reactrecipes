import React from 'react';
import { Route, Link, Redirect } from 'react-router-dom'
import { connect } from 'react-redux';
import PageView from 'containers/PageView';

import * as recipeActions from '../../store/modules/recipes';
import loadRecipesHOC from '../../components/LoadRecipesHOC';
import ViewRecipe from '../../components/ViewRecipe.jsx';
import EditRecipe from '../../components/EditRecipe.jsx';

@connect(state => {
  return {
    isRecipesLoaded: recipeActions.isLoaded(state),
    recipes: state.recipes,
    user: state.auth.user
  };
}, recipeActions)
@loadRecipesHOC
class Recipe extends React.Component {
  constructor(props) {
    super(props);
  }

  onEdit() {
    const id = this.props.match.params.id;
    this.props.history.push(`/recipes/${id}/edit`);
  }
  onDelete() {
    const id = this.props.match.params.id;
    this.props._delete(id);
  }
  onSave(recipeObj) {
    this.props.update(recipeObj);
    this.props.history.push(`/recipes/${recipeObj.id}`);
  }

  render() {
    const {
      recipes: { data },
      match,
      user
    } = this.props;
    const { params: { id } } = match;
    const recipe = data.find(r => r._id === id);

    return (
      <PageView {...this.props}>
      <div>
        <Route exact path={`${match.url}/edit`} render={() => (
          <EditRecipe
            initialValues={recipe}
            user={user}
            onSubmit={this.onSave.bind(this)}
            mode='edit'
          />
        )} />
        <Route exact path={match.url} render={() => (
          recipe ? (
            <ViewRecipe
              recipe={recipe}
              user={user}
              onEdit={this.onEdit.bind(this)}
              onDelete={this.onDelete.bind(this)}
            />
          ) : (
            <Redirect to={{
              pathname: '/',
              state: { from: this.props.location }
            }}/>
          )
        )}/>
      </div>
      </PageView>
    )

    // return recipe ? (
      // <ViewRecipe
      //   {...recipe}
      //   user={user}
      //   onEdit={this.onEdit.bind(this)}
      // />
    // ) : null;
  }
}

export default Recipe;
