import React from 'react';
import { Route, Link, Redirect } from 'react-router-dom'
import { connect } from 'react-redux';
import PageView from 'containers/PageView';

import * as recipeActions from '../../store/modules/recipes';
import EditRecipe from '../../components/EditRecipe.jsx';

@connect(state => {
  return {
    user: state.auth.user
  };
}, recipeActions)
class Recipe extends React.Component {
  constructor(props) {
    super(props);
  }

  onCreate(recipeObj) {
    this.props.create(recipeObj);
    this.props.history.push(`/recipes/${recipeObj.id}`);
  }

  render() {
    const {
      user
    } = this.props;

    return (
      <PageView {...this.props}>
      <EditRecipe
        user={user}
        onSubmit={this.onCreate.bind(this)}
        mode='create'
      />
      </PageView>
    )
  }
}

export default Recipe;
