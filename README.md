# ReactRecipes

## Production Requirements

- [Node.js](https://nodejs.org/)
- [Yarn](https://yarnpkg.com/)

## Development Prerequisites

- [Node.js](https://nodejs.org/)
- [Yarn](https://yarnpkg.com/)
- [Nodemon](https://nodemon.io/)

```bash
npm -g i nodemon yarn
```


## App Configuration

Place a JSON file in the root directory named config.json with the following contents.
Additional settings are automatically populated when the project is built for production.

```json
{
  "port": 3000,
  "hostname": "localhost",
  "analytics_tracking_id": "UA-XXXXXXX-X",
}
```

## Development

### Build/running in development

To install all required npm packages:

```bash
yarn
```

To build vendor JS file run:

```bash
yarn vendor
```

To start the server in dev mode:

```bash
yarn start
```

Monitor for /server changes and auto-restart with:

```bash
yarn watch
```



## Production

### Build for production

```bash
yarn
yarn vendor
yarn release
```

To run the production build ( in unix/linux )

```bash
NODE_ENV=production node server
```
